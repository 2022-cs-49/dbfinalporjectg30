﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="Product.aspx.cs" Inherits="JSM.Admin.Product" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
        window.onload = function () {
            var seconds = 5;
            setTimeout(function () {
                document.getElementById("<% = lblmsg.ClientID %>").style.display = "none";
            }, seconds * 1000)
        }
    </script> 
    <script>
        function ImagePreview1(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#<%=imagePreview.ClientID %>').prop('src', e.target.result)
                        .width(100)
                        .height(100);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="md-4">
        <asp:Label ID="lblmsg" runat="server"></asp:Label>

    </div>
    <div class="row">
        <div class="row">
            <div class="col-sm-20 col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Product</h4>
                        <hr />
                        <label>Product Name *</label>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <asp:TextBox ID="txtProductName" runat="server" CssClass="form-control" placeholder="Enter Product Name"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtProductName" ErrorMessage="Product Name is Required" ForeColor="Red" Display="Dynamic" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                        </div>
                        <label>Short Desciption</label>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <asp:TextBox ID="txtShortDescipiton" runat="server" CssClass="form-control" placeholder="Enter Short Descripiton"></asp:TextBox>
                                    <asp:HiddenField ID="hfProductId" runat="server" Value="0" />

                                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="fuCategoryImage" ErrorMessage="Category Name is Required" ForeColor="Red" Display="Dynamic" SetFocusOnError="true"></asp:RequiredFieldValidator>--%>
                                </div>
                            </div>
                        </div>
                        <label>Long Descripiton</label>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <asp:TextBox ID="txtLongDescription" runat="server" TextMode="MultiLine" CssClass="form-control" placeholder="Enter Long Description"></asp:TextBox>
                                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="fuCategoryImage" ErrorMessage="Category Name is Required" ForeColor="Red" Display="Dynamic" SetFocusOnError="true"></asp:RequiredFieldValidator>--%>
                                </div>
                            </div>
                        </div>
                        <label>Main Picture *</label>
                        <div class="col-md-12">
                            <div class="form-group">
                                <asp:FileUpload ID="pdMainImage" runat="server" CssClass="form-control" AllowMultiple="false" onchange="ImagePreview1(this);" />
                                <asp:HiddenField ID="hfImageId" runat="server" Value="0" />
                                <asp:HiddenField ID="hfCategoryId" runat="server" Value="0" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="pdMainImage" ErrorMessage="Main Picture Required Atleast" ForeColor="Red" Display="Dynamic" SetFocusOnError="true"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div>
                            <asp:Image ID="imagePreview" runat="server" CssClass="img-thumbnail" AlternateText="Main Image" />
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <asp:CheckBox ID="cbDefault" runat="server" Text="&nbsp; Default" />
                                </div>
                            </div>

                        </div>
                        <label>Additional Descripiton</label>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <asp:TextBox ID="txtAdditionalDescription" runat="server" TextMode="MultiLine" CssClass="form-control" placeholder="Enter Additional Description"></asp:TextBox>
                                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="fuCategoryImage" ErrorMessage="Category Name is Required" ForeColor="Red" Display="Dynamic" SetFocusOnError="true"></asp:RequiredFieldValidator>--%>
                                </div>
                            </div>
                        </div>
                        <label>Price *</label>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <asp:TextBox ID="txtPrice" runat="server" CssClass="form-control" placeholder="Enter Price"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtPrice" ErrorMessage="Category Name is Required" ForeColor="Red" Display="Dynamic" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                        </div>
                        <label>Quantity *</label>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <asp:TextBox ID="txtQuantity" runat="server" CssClass="form-control" placeholder="Enter Quantity"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtQuantity" ErrorMessage="Qunatity is Required" ForeColor="Red" Display="Dynamic" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                        </div>
                        <label>Size</label>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <asp:DropDownList ID="txtSize" runat="server" CssClass="form-control">
                                        <asp:ListItem Text="S" Value="S"></asp:ListItem>
                                        <asp:ListItem Text="M" Value="M"></asp:ListItem>
                                        <asp:ListItem Text="L" Value="L"></asp:ListItem>
                                        <asp:ListItem Text="XL" Value="XL"></asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtSize" ErrorMessage="Size is Required" ForeColor="Red" Display="Dynamic" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                        </div>

                        <label>Color</label>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <asp:DropDownList ID="txtColor" runat="server" CssClass="form-control" SelectionMode="Single">
                                        <asp:ListItem Text="Black" Value="Black"></asp:ListItem>
                                        <asp:ListItem Text="Blue" Value="Blue"></asp:ListItem>
                                        <asp:ListItem Text="White" Value="White"></asp:ListItem>
                                        <asp:ListItem Text="Orange" Value="Orange"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <label>Manufacturer</label>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <asp:TextBox ID="txtManufacturer" runat="server" CssClass="form-control" placeholder="Enter Manufacturer"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <label>Category *</label>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <asp:DropDownList ID="txtCategory" runat="server" CssClass="form-control">
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtCategory" ErrorMessage="Category Name is Required" ForeColor="Red" Display="Dynamic" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <asp:CheckBox ID="cbSold" runat="server" Text="&nbsp; Sold" />
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <asp:CheckBox ID="cbIsCustomized" runat="server" Text="&nbsp; Customized" />
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <asp:CheckBox ID="cbIsActive" runat="server" Text="&nbsp; Active" />
                                </div>
                            </div>
                        </div>
                        <div class="form-action pb-5">
                            <div class="text-left">
                                <asp:Button ID="btnAddOrUpdate" runat="server" CssClass="btn btn-info" Text="Add" OnClick="btnAddOrUpdate_Click" />
                                <asp:Button ID="btnCancel" runat="server" CssClass="btn btn-danger" Text="Reset" OnClick="btnCancel_Click" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-10 col-md-6">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Products List</h4>
                    <hr />
                    <div class="table-responsive">
                        <asp:Repeater ID="pdTable" runat="server" OnItemCommand="pdTable_ItemCommand">
                            <HeaderTemplate>
                                <table class="table data-table-export table-hover nowrap">
                                    <thead>
                                        <tr>
                                            <th class="table-plus">Name</th>
                                            <th>Price</th>
                                            <th>Quantity</th>
                                            <th>IsActive</th>
                                            <th>Sold Out</th>
                                            <th class="datatable-nosort">Created Date</th>
                                            <th class="datatable-nosort">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td class="table-plus"><%# Eval("ProductName") %></td>
                                    <td class="table-plus">
                                        <%# Eval("Price") %>
                                        <%--<img width="40" src="<%# JSM.Utils.getImageUrl(Eval("CategoryImageUrl")) %>" alt="image" />--%>
                                    </td>
                                    <td class="table-plus"><%# Eval("Quantity") %></td>

                                    <td>
                                        <asp:Label ID="lblIsActive" runat="server" Text='<%# (bool) Eval("IsActive") == true ? "Active": "InActive" %>' CssClass='<%# (bool) Eval("IsActive") == true ? "badge badge-success": "badge badge-danger" %>'></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblIsSold" runat="server"
                                            Text='<%# Eval("Sold") != DBNull.Value && Convert.ToBoolean(Eval("Sold")) == true ? "Yes" : "No" %>'
                                            CssClass='<%# Eval("Sold") != DBNull.Value && Convert.ToBoolean(Eval("Sold")) == true ? "badge badge-success" : "badge badge-danger" %>'>
                                        </asp:Label>
                                        <%--<asp:Label ID="lblIsSold" runat="server" Text='<%# (bool) Eval("Sold") == true ? "Active": "InActive" %>' CssClass='<%# (bool) Eval("Sold") == true ? "badge badge-success": "badge badge-danger" %>'></asp:Label>--%>
                                    </td>
                                    <td>
                                        <%# Eval("CreatedDate") %>
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="lblEdit" runat="server" Text="Edit" CssClass="badge badge-primary"
                                            CommandArgument='<%# Eval("ProductID") %>' CommandName="edit" CausesValidation="false">
                                       <i class ="fas fa-edit">
                                       </i>
                                        </asp:LinkButton>
                                        <asp:LinkButton ID="lblDelete" runat="server" Text="Delete" CssClass="badge badge-danger"
                                            CommandArgument='<%# Eval("ProductID") %>' CommandName="delete" CausesValidation="false">
                                    <i class ="fas fa-trash-alt"></i>
                                        </asp:LinkButton>

                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </tbody>
                        </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
