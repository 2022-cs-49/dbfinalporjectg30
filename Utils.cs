﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Web;
using System.Configuration;

namespace JSM
{

    public class Utils
    {
        SqlConnection con;
        SqlCommand cmd;
        SqlDataAdapter sda;
        DataTable da;
        SqlDataReader sdr;
        public static string getConnection()
        {
            return ConfigurationManager.ConnectionStrings["cs"].ConnectionString;
        }
        public static bool isValidExtension(string fileName)
        {
            bool isvalid = false;
            string[] validExtensions = { ".jpg", ".jpeg", ".png" };
            foreach (string extension in validExtensions)
            {
                if (fileName.Contains(extension))
                {
                    isvalid = true;
                    break;
                }
            }
            return isvalid;
        }

        public static string getUniqueId()
        {
            Guid guid = Guid.NewGuid();
            return guid.ToString();
            
        }

        public static string getImageUrl(Object url)
        {
            string url1 = string.Empty;
            if (string.IsNullOrEmpty(url.ToString()) || url == DBNull.Value)
            {
                url1 = "../Images/Category/no_image.jpg";
            }
            else
            {
                url1 = string.Format("../{0}", url);
            }
            return url1;
        }
    }
}