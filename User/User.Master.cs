﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace JSM.User
{
    public partial class User : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Url.AbsoluteUri.ToString().Contains("Default.aspx"))
            {
                Control slideControl = (Control)Page.LoadControl("SliderUserControl.ascx");
                SliderUC.Controls.Add(slideControl);
            }
        }
        public DataTable GetAllCategories()
        {
            DataTable categoriesTable = new DataTable();
            using (SqlConnection connection = new SqlConnection(Utils.getConnection()))
            {
                using (SqlCommand command = new SqlCommand("Category_Crud", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@Action", "GETALL");

                    connection.Open();
                    SqlDataAdapter adapter = new SqlDataAdapter(command);
                    adapter.Fill(categoriesTable);
                }
            }
            return categoriesTable;
        }
    }
}