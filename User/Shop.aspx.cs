﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using JSM.Admin;

namespace JSM.User
{
    public partial class Shop : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                DataTable productsTable = LoadProductsByCategory();       
                ProductRepeater.DataSource = productsTable;
                ProductRepeater.DataBind();
            }
        }

        public DataTable LoadProductsByCategory()
        {
            DataTable categoriesTable = new DataTable();
            using (SqlConnection connection = new SqlConnection(Utils.getConnection()))
            {
                using (SqlCommand command = new SqlCommand("Product_Crud", connection))
                {

                    if (Request.QueryString["CategoryID"] != null)
                    {
                        int categoryId = Convert.ToInt32(Request.QueryString["CategoryID"]);
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@Action", "GETWITHIMAGE");
                        command.Parameters.AddWithValue("@CategoryID", categoryId);
                        connection.Open();
                        SqlDataAdapter adapter = new SqlDataAdapter(command);
                        adapter.Fill(categoriesTable);
                    }

                }
            }
            return categoriesTable;
        }
        protected DataTable filterByPrice(decimal minPrice, decimal maxPrice)
        {
            // Assuming you're using ADO.NET to interact with your database
            DataTable productTable = new DataTable();

            using (SqlConnection connection = new SqlConnection(Utils.getConnection()))
            {
                string query = "SELECT * FROM Products WHERE Price BETWEEN @MinPrice AND @MaxPrice";
                SqlCommand command = new SqlCommand(query, connection);
                command.Parameters.AddWithValue("@MinPrice", minPrice);
                command.Parameters.AddWithValue("@MaxPrice", maxPrice);

                SqlDataAdapter adapter = new SqlDataAdapter(command);
                adapter.Fill(productTable);
            }

            return productTable;
        }

        protected string GetSelectedPriceRange()
        {
            string selectedPriceRange = string.Empty;

            foreach (ListItem item in PriceCheckBoxList.Items)
            {
                if (item.Selected)
                {
                    selectedPriceRange = item.Text;
                    break;
                }
            }

            return selectedPriceRange;
        }

        // Method to get the selected color
        protected string GetSelectedColor()
        {
            string selectedColor = string.Empty;

            foreach (ListItem item in ColorCheckBoxList.Items)
            {
                if (item.Selected)
                {
                    selectedColor = item.Text;
                    break;
                }
            }

            return selectedColor;
        }

        // Method to get the selected size
        protected string GetSelectedSize()
        {
            string selectedSize = string.Empty;

            foreach (ListItem item in SizeCheckBoxList.Items)
            {
                if (item.Selected)
                {
                    selectedSize = item.Text;
                    break;
                }
            }

            return selectedSize;
        }

        protected void btnAddOrUpdate_Click(object sender, EventArgs e)
        {
            // Get selected price range
            string selectedPriceRange = GetSelectedPriceRange();

            // Get selected color
            string selectedColor = GetSelectedColor();

            // Get selected size
            string selectedSize = GetSelectedSize();
            DataTable filteredProducts = FilterProducts(selectedPriceRange, selectedColor, selectedSize);
            if (filteredProducts != null && filteredProducts.Rows.Count > 0)
            {
                // Bind the filtered products to the GridView
                ProductRepeater.DataSource = filteredProducts;
                ProductRepeater.DataBind();
            }
        }
        protected DataTable FilterProducts(string priceRange, string color, string size)
        {
            DataTable allProducts = LoadProductsByCategory();
            DataTable filteredProducts = new DataTable();
            filteredProducts.Columns.Add("ProductName", typeof(string));
            filteredProducts.Columns.Add("Price", typeof(decimal));
            filteredProducts.Columns.Add("ImageUrl", typeof(string));
            filteredProducts.Columns.Add("ProductId", typeof(int));
            foreach (DataRow row in allProducts.Rows)
            {
                // Assuming your product table has columns like Price, Color, and Size
                decimal productPrice = Convert.ToDecimal(row["Price"]);
                string productColor = row["Color"].ToString();
                string productSize = row["Size"].ToString();
                string productImage = row["ImageUrl"].ToString();
                int productId = Convert.ToInt32(row["ProductId"]);
                string productName = row["ProductName"].ToString();
                // Check if the product matches the filter criteria
                if ((string.IsNullOrEmpty(priceRange) || CheckPriceRange(productPrice, priceRange)) &&
                    (string.IsNullOrEmpty(color) || productColor.Equals(color, StringComparison.OrdinalIgnoreCase)) &&
                    (string.IsNullOrEmpty(size) || productSize.Equals(size, StringComparison.OrdinalIgnoreCase)))
                {
                    // If the product matches the criteria, add it to the filteredProducts DataTable
                    filteredProducts.Rows.Add(productName, productPrice, productImage, productId);
                    //filteredProducts.ImportRow(row);
                }
            }

            return filteredProducts;

        }
        private bool CheckPriceRange(decimal productPrice, string priceRange)
        {
            // Split the price range string into minimum and maximum values
            string[] rangeValues = priceRange.Split(new char[] { '-' }, StringSplitOptions.RemoveEmptyEntries);

            if (rangeValues.Length == 2)
            {
                // Parse the minimum and maximum values
                if (decimal.TryParse(rangeValues[0].Trim(), out decimal minPrice) &&
                    decimal.TryParse(rangeValues[1].Trim(), out decimal maxPrice))
                {
                    // Check if the product price falls within the specified range
                    if (productPrice >= minPrice && productPrice <= maxPrice)
                    {
                        return true; // Price falls within the range
                    }
                }
            }

            return false; // Price does not fall within the range or invalid range format
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {

        }
    }
}