﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;


namespace JSM.Admin
{
    public partial class Product : System.Web.UI.Page
    {
        SqlConnection con;
        SqlCommand cmd;
        SqlDataAdapter sda;
        DataTable da;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                PopulateCategoryDropDownList();
            }
            GetProductCategory();
        }

        protected void btnAddOrUpdate_Click(object sender, EventArgs e)
        {
            string selectedSizes = GetSelectedValues(SizeCheckBox);
            string selectedColors = GetSelectedValues(ColorChekcBox);
            int price = 0;
            int quantity = 0;
            string fileExtension = string.Empty, mainImage = string.Empty;
            if (checkIfInT(txtPrice.Text.Trim()) && checkIfInT(txtQuantity.Text.Trim()))
            {
                price = ConvertToInt32(txtPrice.Text.Trim());
                quantity = ConvertToInt32(txtQuantity.Text.Trim());
            }
            string selectedCategoryName = txtCategory.SelectedItem.Text;
            Category selectedCategory = GetCategories().FirstOrDefault(cat => cat.CategoryName == selectedCategoryName);
            if (selectedCategory != null)
            {
                int categoryID = selectedCategory.CategoryId;
                string actionName = string.Empty;
                int ProductID = Convert.ToInt32(hfProductId.Value);
                using (SqlConnection con = new SqlConnection(Utils.getConnection()))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand("Product_Crud", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@Action", SqlDbType.VarChar, 15).Value = hfProductId.Value == "0" ? "INSERT" : "UPDATE";
                        cmd.Parameters.AddWithValue("@ProductID", ProductID);
                        cmd.Parameters.AddWithValue("@ProductName", txtProductName.Text.Trim());
                        cmd.Parameters.AddWithValue("@ShortDescription", txtShortDescipiton.Text.Trim());
                        cmd.Parameters.AddWithValue("@Price", price);
                        cmd.Parameters.AddWithValue("@LongDescription", txtLongDescription.Text.Trim());
                        cmd.Parameters.AddWithValue("@AdditionalDescription", txtAdditionalDescription.Text);
                        cmd.Parameters.AddWithValue("@Size", selectedSizes);
                        cmd.Parameters.AddWithValue("@Color", selectedColors);
                        cmd.Parameters.AddWithValue("@CompanyName", txtManufacturer.Text.Trim());
                        cmd.Parameters.AddWithValue("@Quantity", quantity);
                        cmd.Parameters.AddWithValue("@IsActive", cbIsActive.Checked);
                        cmd.Parameters.AddWithValue("@Sold", cbSold.Checked);
                        cmd.Parameters.AddWithValue("@IsCustomized", cbIsCustomized.Checked);
                        cmd.Parameters.AddWithValue("@CategoryID", categoryID);

                        try
                        {
                            cmd.ExecuteNonQuery();
                            SqlCommand cmd2 = new SqlCommand("Product_Crud", con);
                            cmd2.Parameters.AddWithValue("@Action", "GETPRODUCTID");
                            cmd2.CommandType = CommandType.StoredProcedure;
                            SqlDataAdapter sda1;
                            DataTable da1;
                            sda1 = new SqlDataAdapter(cmd2);
                            da1 = new DataTable();
                            sda1.Fill(da1);
                            int ProductId = Convert.ToInt32(da1.Rows[0]["ProductId"]);
                            if (hfProductId.Value == "0")
                            {
                                // Prepare SQL command for Image CRUD
                                using (SqlCommand cmd1 = new SqlCommand("PicturesCrud", con))
                                {

                                    cmd1.CommandType = CommandType.StoredProcedure;
                                    cmd1.Parameters.Add("@Action", SqlDbType.VarChar, 10).Value = ProductID == 0 ? "INSERT" : "UPDATE";
                                    string newImageName2 = Utils.getUniqueId();
                                    fileExtension = Path.GetExtension(pdMainImage.FileName);
                                    mainImage = "../Images/Product/" + newImageName2.ToString() + fileExtension;
                                    pdMainImage.PostedFile.SaveAs(Server.MapPath("../Images/Product/") + newImageName2.ToString() + fileExtension);
                                    cmd1.Parameters.AddWithValue("@ProductID", ProductId);
                                    cmd1.Parameters.AddWithValue("@DefaultImageUrl", cbDefault.Checked);
                                    cmd1.Parameters.AddWithValue("@ImageUrl", mainImage);
                                    cmd1.ExecuteNonQuery();
                                }
                            }
                            using (SqlCommand cmdInsertImages = new SqlCommand("INSERT INTO productAddImages (ProductId, image1, image2, image3, image4) VALUES (@ProductId, @Image1, @Image2, @Image3, @Image4)", con))
                            {
                                cmdInsertImages.Parameters.AddWithValue("@ProductId", ProductId);
                                HttpFileCollection uploadedFiles = Request.Files;

                                // Add parameters for up to 4 images
                                for (int i = 0; i < 4; i++)
                                {
                                    if (i < uploadedFiles.Count && uploadedFiles[i] != null && uploadedFiles[i].ContentLength > 0 && Utils.isValidExtension(uploadedFiles[i].FileName))
                                    {
                                        string imageName = Utils.getUniqueId();
                                        string fileExtension1 = Path.GetExtension(uploadedFiles[i].FileName);
                                        string imagePath = "../Images/Product/" + imageName + fileExtension1;
                                        uploadedFiles[i].SaveAs(Server.MapPath(imagePath));
                                        cmdInsertImages.Parameters.AddWithValue($"@Image{i + 1}", imagePath);
                                    }
                                    else
                                    {
                                        // If fewer than 4 images uploaded, fill remaining parameters with DBNull.Value
                                        cmdInsertImages.Parameters.AddWithValue($"@Image{i + 1}", DBNull.Value);
                                    }
                                }

                                cmdInsertImages.ExecuteNonQuery();
                            }
                            // Display success message
                            lblmsg.Visible = true;
                            lblmsg.Text = "Product " + (hfProductId.Value == "0" ? "Inserted" : "Updated") + " Successfully";
                            lblmsg.CssClass = "alert alert-success";
                            CLEARDATA();
                            GetProductCategory();
                        }
                        catch (Exception ex)
                        {
                            // Display error message
                            lblmsg.Visible = true;
                            lblmsg.Text = "Error: " + ex.Message;
                            lblmsg.CssClass = "alert alert-danger";
                        }
                    }
                }



            }
        }

        private string GetSelectedValues(CheckBoxList checkBoxList)
        {
            List<string> selectedValues = new List<string>();
            foreach (ListItem item in checkBoxList.Items)
            {
                if (item.Selected)
                {
                    selectedValues.Add(item.Value);
                }
            }
            return string.Join(",", selectedValues);
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            CLEARDATA();
        }
        int ConvertToInt32(string value)
        {
            return Convert.ToInt32(value);
        }
        private void PopulateCategoryDropDownList()
        {
            List<Category> categories = GetCategories(); // GetCategories() returns a List<Category>
            txtCategory.DataSource = categories;
            txtCategory.DataTextField = "CategoryName"; // Assuming your Category class has a property CategoryName
            txtCategory.DataValueField = ""; // Set the DataValueField to an empty string since you only want to display the CategoryName
            txtCategory.DataBind();
            txtCategory.Items.Insert(0, new ListItem("Select Category", ""));
        }
        bool checkIfInT(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                value = "0";
            }
            try
            {
                int.Parse(value);

                return true;
            }
            catch (FormatException)
            {
                lblmsg.Visible = true;
                lblmsg.Text = "Please enter a valid number";
                lblmsg.CssClass = "alert alert-danger";
                return false;
            }
        }
        private List<Category> GetCategories()
        {
            List<Category> categories = new List<Category>();
            DataTable dt = new DataTable();
            using (SqlConnection con = new SqlConnection(Utils.getConnection()))
            {
                using (SqlCommand cmd = new SqlCommand("Category_Crud", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Action", "GETALL");
                    using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                    {
                        sda.Fill(dt);
                    }
                }
            }

            foreach (DataRow row in dt.Rows)
            {
                Category category = new Category
                {
                    CategoryId = Convert.ToInt32(row["CategoryId"]),
                    CategoryName = row["CategoryName"].ToString()
                };
                categories.Add(category);
            }

            return categories;
        }

        void GetProductCategory()
        {
            pdTable.DataSource = null;
            pdTable.DataBind();
            con = new SqlConnection(Utils.getConnection());
            cmd = new SqlCommand("Product_Crud", con);
            cmd.Parameters.AddWithValue("@Action", "GETALL");
            cmd.CommandType = CommandType.StoredProcedure;
            sda = new SqlDataAdapter(cmd);
            da = new DataTable();
            sda.Fill(da);
            pdTable.DataSource = da;
            pdTable.DataBind();
        }
        void CLEARDATA()
        {
            try
            {
                txtProductName.Text = string.Empty;
                txtShortDescipiton.Text = string.Empty;
                txtPrice.Text = string.Empty;
                txtLongDescription.Text = string.Empty;
                txtAdditionalDescription.Text = string.Empty;
                ColorChekcBox.Text = string.Empty;
                SizeCheckBox.Text = string.Empty;
                txtManufacturer.Text = string.Empty;
                txtQuantity.Text = string.Empty;
                cbSold.Checked = false;
                cbIsCustomized.Checked = false;
                cbIsActive.Checked = false;
            }
            catch (Exception ex)
            {
                lblmsg.Visible = true;
                lblmsg.Text = "Error: " + ex.Message;
                lblmsg.CssClass = "alert alert-danger";
            }

        }
        protected void pdTable_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            lblmsg.Visible = false;
            if (e.CommandName == "edit")
            {
                CLEARDATA();
                con = new SqlConnection(Utils.getConnection());
                SqlCommand cmd1 = new SqlCommand("PicturesCrud", con);
                cmd = new SqlCommand("Product_Crud", con);
                cmd1.Parameters.AddWithValue("@ProductId", e.CommandArgument);
                cmd1.Parameters.AddWithValue("@Action", "GETBYID");
                cmd1.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter sda1;
                DataTable da1;
                sda1 = new SqlDataAdapter(cmd1);
                da1 = new DataTable();
                sda1.Fill(da1);
                hfImageId.Value = da1.Rows[0]["ImageId"].ToString();
                //pdMainImage.AccessKey = da1.Rows[0]["ImageUrl"].ToString();
                cmd.Parameters.AddWithValue("@Action", "GETBYID");
                cmd.Parameters.AddWithValue("@ProductID", e.CommandArgument);
                cmd.CommandType = CommandType.StoredProcedure;
                sda = new SqlDataAdapter(cmd);
                da = new DataTable();
                sda.Fill(da);
                btnAddOrUpdate.Text = "Update";
                txtProductName.Text = da.Rows[0]["ProductName"].ToString();
                txtShortDescipiton.Text = da.Rows[0]["ShortDescription"].ToString();
                txtPrice.Text = Convert.ToInt32(da.Rows[0]["Price"]).ToString();
                txtLongDescription.Text = da.Rows[0]["LongDescription"].ToString();
                txtAdditionalDescription.Text = da.Rows[0]["AdditionalDescription"].ToString();
                string selectedSizes = da.Rows[0]["Size"].ToString();
                string selectedColors = da.Rows[0]["Color"].ToString();
                // Check the sizes and colors in the CheckBoxLists
                CheckSelectedValues(selectedSizes, SizeCheckBox);
                CheckSelectedValues(selectedColors, ColorChekcBox);
                txtManufacturer.Text = da.Rows[0]["CompanyName"].ToString();
                txtQuantity.Text = da.Rows[0]["Quantity"].ToString();
                cbSold.Checked = Convert.ToBoolean(da.Rows[0]["Sold"]);
                cbIsCustomized.Checked = Convert.ToBoolean(da.Rows[0]["IsCustiomized"]);
                cbIsActive.Checked = Convert.ToBoolean(da.Rows[0]["IsActive"]);
                hfProductId.Value = da.Rows[0]["ProductID"].ToString();
            }
            else if (e.CommandName == "delete")
            {
                con = new SqlConnection(Utils.getConnection());
                SqlCommand cmd1 = new SqlCommand("PicturesCrud", con);
                cmd = new SqlCommand("Product_Crud", con);
                cmd.Parameters.AddWithValue("@Action", "DELETE");
                cmd1.Parameters.AddWithValue("@Action", "DELETE");
                cmd1.Parameters.AddWithValue("@ProductID", e.CommandArgument);
                cmd.Parameters.AddWithValue("@ProductID", e.CommandArgument);
                cmd.CommandType = CommandType.StoredProcedure;
                try
                {
                    con.Open();
                    cmd.ExecuteNonQuery();
                    cmd1.ExecuteNonQuery();
                    lblmsg.Visible = true;
                    lblmsg.Text = "Category Deleted Successfully";
                    lblmsg.CssClass = "alert alert-danger";
                }
                catch (Exception ex)
                {
                    lblmsg.Visible = true;
                    lblmsg.Text = "Error: " + ex.Message;
                    lblmsg.CssClass = "alert alert-danger";
                }
                finally
                {
                    CLEARDATA();
                    GetProductCategory();
                    con.Close();
                }
            }
        }
        public class Category
        {
            public int CategoryId { get; set; }
            public string CategoryName { get; set; }

        }
        private void CheckSelectedValues(string selectedValues, CheckBoxList checkBoxList)
        {
            foreach (ListItem item in checkBoxList.Items)
            {
                if (selectedValues.Contains(item.Value))
                {
                    item.Selected = true;
                }
            }
        }
        protected void btnUpload_Click(object sender, EventArgs e)
        {

        }
    }
}