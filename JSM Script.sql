USE [master]
GO
/****** Object:  Database [JSM]    Script Date: 5/3/2024 6:11:20 PM ******/
CREATE DATABASE [JSM]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'JSM', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL16.MSSQLSERVER\MSSQL\DATA\JSM.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'JSM_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL16.MSSQLSERVER\MSSQL\DATA\JSM_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT, LEDGER = OFF
GO
ALTER DATABASE [JSM] SET COMPATIBILITY_LEVEL = 160
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [JSM].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [JSM] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [JSM] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [JSM] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [JSM] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [JSM] SET ARITHABORT OFF 
GO
ALTER DATABASE [JSM] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [JSM] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [JSM] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [JSM] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [JSM] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [JSM] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [JSM] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [JSM] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [JSM] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [JSM] SET  ENABLE_BROKER 
GO
ALTER DATABASE [JSM] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [JSM] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [JSM] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [JSM] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [JSM] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [JSM] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [JSM] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [JSM] SET RECOVERY FULL 
GO
ALTER DATABASE [JSM] SET  MULTI_USER 
GO
ALTER DATABASE [JSM] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [JSM] SET DB_CHAINING OFF 
GO
ALTER DATABASE [JSM] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [JSM] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [JSM] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [JSM] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
EXEC sys.sp_db_vardecimal_storage_format N'JSM', N'ON'
GO
ALTER DATABASE [JSM] SET QUERY_STORE = ON
GO
ALTER DATABASE [JSM] SET QUERY_STORE (OPERATION_MODE = READ_WRITE, CLEANUP_POLICY = (STALE_QUERY_THRESHOLD_DAYS = 30), DATA_FLUSH_INTERVAL_SECONDS = 900, INTERVAL_LENGTH_MINUTES = 60, MAX_STORAGE_SIZE_MB = 1000, QUERY_CAPTURE_MODE = AUTO, SIZE_BASED_CLEANUP_MODE = AUTO, MAX_PLANS_PER_QUERY = 200, WAIT_STATS_CAPTURE_MODE = ON)
GO
USE [JSM]
GO
/****** Object:  Table [dbo].[Cart]    Script Date: 5/3/2024 6:11:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cart](
	[CartId] [int] IDENTITY(1,1) NOT NULL,
	[ProductId] [int] NOT NULL,
	[UserId] [int] NOT NULL,
	[Quantity] [int] NULL,
	[CreatedDate] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[CartId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Category]    Script Date: 5/3/2024 6:11:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Category](
	[CategoryID] [int] IDENTITY(1,1) NOT NULL,
	[CategoryName] [varchar](100) NOT NULL,
	[CategoryImageUrl] [varchar](max) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[CategoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Contact]    Script Date: 5/3/2024 6:11:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Contact](
	[ContactId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
	[Email] [varchar](50) NULL,
	[Subject] [varchar](200) NULL,
	[Message] [varchar](max) NULL,
	[CreatedDate] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ContactId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Orders]    Script Date: 5/3/2024 6:11:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Orders](
	[OrderDetailsId] [int] IDENTITY(1,1) NOT NULL,
	[OrderNo] [varchar](max) NULL,
	[ProductId] [int] NOT NULL,
	[Quantity] [int] NULL,
	[UserId] [int] NOT NULL,
	[Status] [varchar](50) NULL,
	[PaymentId] [int] NOT NULL,
	[OrderDate] [datetime] NOT NULL,
	[IsCancel] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[OrderDetailsId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Payment]    Script Date: 5/3/2024 6:11:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Payment](
	[PaymentId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
	[CardNo] [varchar](50) NULL,
	[ExpiryDate] [varchar](50) NULL,
	[CvvNo] [int] NULL,
	[Adress] [varchar](50) NULL,
	[PaymentMode] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[PaymentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Product]    Script Date: 5/3/2024 6:11:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Product](
	[ProductId] [int] IDENTITY(1,1) NOT NULL,
	[ProductName] [varchar](100) NOT NULL,
	[ShortDescription] [varchar](200) NULL,
	[LongDescription] [varchar](max) NULL,
	[AdditionalDescription] [varchar](max) NULL,
	[Price] [decimal](18, 2) NOT NULL,
	[Quantity] [int] NOT NULL,
	[Size] [varchar](30) NULL,
	[Color] [varchar](50) NULL,
	[CompanyName] [varchar](100) NULL,
	[CategoryId] [int] NOT NULL,
	[Sold] [int] NULL,
	[IsCustiomized] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ProductId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[productAddImages]    Script Date: 5/3/2024 6:11:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[productAddImages](
	[ProductId] [int] NULL,
	[image1] [varchar](max) NULL,
	[image2] [varchar](max) NULL,
	[image3] [varchar](max) NULL,
	[image4] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ProductImages]    Script Date: 5/3/2024 6:11:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductImages](
	[ImageId] [int] IDENTITY(1,1) NOT NULL,
	[ImageUrl] [varchar](max) NOT NULL,
	[ProductId] [int] NOT NULL,
	[DefaultImage] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[ImageId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ProductReview]    Script Date: 5/3/2024 6:11:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductReview](
	[ReviewId] [int] IDENTITY(1,1) NOT NULL,
	[Rating] [int] NOT NULL,
	[Comment] [varchar](max) NULL,
	[ProductId] [int] NOT NULL,
	[UserId] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ReviewId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Roles]    Script Date: 5/3/2024 6:11:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Roles](
	[RoleId] [int] NOT NULL,
	[RoleName] [varchar](20) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SubCategory]    Script Date: 5/3/2024 6:11:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SubCategory](
	[CategoryID] [int] NOT NULL,
	[SubCategoryID] [int] IDENTITY(1,1) NOT NULL,
	[SubCategoryName] [varchar](100) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[SubCategoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Users]    Script Date: 5/3/2024 6:11:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[UserId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
	[Username] [varchar](50) NULL,
	[Mobile] [varchar](20) NULL,
	[Email] [varchar](50) NULL,
	[Adress] [varchar](max) NULL,
	[PostCode] [varchar](50) NULL,
	[Password] [varchar](50) NULL,
	[ImageUrl] [varchar](max) NULL,
	[RoleId] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[Username] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Wishlist]    Script Date: 5/3/2024 6:11:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Wishlist](
	[WishlistId] [int] IDENTITY(1,1) NOT NULL,
	[ProductId] [int] NOT NULL,
	[UserId] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[WishlistId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Orders] ADD  DEFAULT ((0)) FOR [IsCancel]
GO
ALTER TABLE [dbo].[Cart]  WITH CHECK ADD FOREIGN KEY([ProductId])
REFERENCES [dbo].[Product] ([ProductId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Cart]  WITH CHECK ADD FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([UserId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Orders]  WITH CHECK ADD FOREIGN KEY([PaymentId])
REFERENCES [dbo].[Payment] ([PaymentId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Orders]  WITH CHECK ADD FOREIGN KEY([ProductId])
REFERENCES [dbo].[Product] ([ProductId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Orders]  WITH CHECK ADD FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([UserId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Product]  WITH CHECK ADD FOREIGN KEY([CategoryId])
REFERENCES [dbo].[Category] ([CategoryID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[productAddImages]  WITH CHECK ADD FOREIGN KEY([ProductId])
REFERENCES [dbo].[Product] ([ProductId])
GO
ALTER TABLE [dbo].[ProductImages]  WITH CHECK ADD FOREIGN KEY([ProductId])
REFERENCES [dbo].[Product] ([ProductId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ProductReview]  WITH CHECK ADD FOREIGN KEY([ProductId])
REFERENCES [dbo].[Product] ([ProductId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ProductReview]  WITH CHECK ADD FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([UserId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[SubCategory]  WITH CHECK ADD FOREIGN KEY([CategoryID])
REFERENCES [dbo].[Category] ([CategoryID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Users]  WITH CHECK ADD FOREIGN KEY([RoleId])
REFERENCES [dbo].[Roles] ([RoleId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Wishlist]  WITH CHECK ADD FOREIGN KEY([ProductId])
REFERENCES [dbo].[Product] ([ProductId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Wishlist]  WITH CHECK ADD FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([UserId])
ON DELETE CASCADE
GO
/****** Object:  StoredProcedure [dbo].[Category_Crud]    Script Date: 5/3/2024 6:11:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Category_Crud]
	@Action VARCHAR(15),
	@CategoryId INT = NULL,
	@CategoryName VARCHAR(100) = NULL,
	@CategoryImageUrl VARCHAR(MAX) = NULL,
	@IsActive BIT = False
	
AS
BEGIN
	SET NOCOUNT ON;

    -- Get all category
	if (@Action = 'GETALL')
	BEGIN
		Select * From Category
	END

	if(@Action = 'GETBYID')
	BEGIN
	Select * from Category c
	Where c.CategoryID = @CategoryId;
	END

	-- Insert category
	if (@Action = 'INSERT')
	BEGIN
	INSERT  INTO Category (CategoryName,CategoryImageUrl,IsActive,CreatedDate)
	VALUES(@CategoryName, @CategoryImageUrl,@IsActive,GETDATE())
	END
	-- Update category
	if (@Action = 'UPDATE')
	BEGIN
	DECLARE @UPDATE_IMAGE VARCHAR(20)
	SELECT @UPDATE_IMAGE = (CASE WHEN @CategoryImageUrl is NULL THEN 'NO' ELSE 'YES' END)
	if (@UPDATE_IMAGE = 'NO')
		BEGIN
			UPDATE   Category 
			SET CategoryName = @CategoryName, IsActive = @IsActive
			WHERE CategoryID = @CategoryId
		END
	ELSE
		BEGIN
			UPDATE   Category 
			SET CategoryName = @CategoryName,@CategoryImageUrl= @CategoryImageUrl, IsActive = @IsActive
			WHERE CategoryID = @CategoryId
		END
	END
	-- Delete Category 
	if (@Action = 'DELETE')
	BEGIN
		DELETE From Category
		WHERE CategoryID = @CategoryId
	END
	-- Get Active Category From Category

	if (@Action = 'ACTIVECATEGORY')
	BEGIN
		SELECT * From Category c
		WHERE c.IsActive = 1 ORDER BY c.CategoryName
	END
END
GO
/****** Object:  StoredProcedure [dbo].[PicturesCrud]    Script Date: 5/3/2024 6:11:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PicturesCrud] 
	@Action VARCHAR(10) = NULL,
	@ImageId INT = NULL,
	@ImageUrl VARCHAR(MAX) = NULL,
	@ProductId INT = NULL,
	@DefaultImageUrl bit = false,
	@Image1 VARCHAR(MAX) = NULL,
	@Image2 VARCHAR(MAX) = NULL,
	@Image3 VARCHAR(MAX) = NULL,
	@Image4 VARCHAR(MAX) = NULL
	
AS
BEGIN
	SET NOCOUNT ON;
	if(@Action = 'INSERT')
	BEGIN
	INSERT INTO ProductImages (ImageUrl, ProductId, DefaultImage)
VALUES (@ImageUrl, @ProductId, @DefaultImageUrl)
	END
	if(@Action = 'GETBYID')
	BEGIN
	Select * from ProductImages
	Where ProductId = @ProductId
	END
	if(@Action = 'GETALL')
	BEGIN
	Select * from ProductImages
	END
	If (@Action = 'DELETE')
	BEGIN
	Delete from ProductImages
	Where ProductId = @ProductId
	END
	if(@Action = 'CHECK')
	BEGIN
	SELECT ProductId FROM Product WHERE @ProductId = ProductId 
	END
	IF(@Action = 'UPDATE')
BEGIN
    UPDATE ProductImages
    SET 
	ImageUrl = @ImageUrl,
        DefaultImage = @DefaultImageUrl
    WHERE ProductId = @ProductId
END
END
GO
/****** Object:  StoredProcedure [dbo].[Product_Crud]    Script Date: 5/3/2024 6:11:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Product_Crud]
	@Action VARCHAR(15),
	@ProductID INT = NULL,
	@ProductName VARCHAR(100) = NULL,
	@ShortDescription VARCHAR(100) = NULL,
	@LongDescription VARCHAR(MAX) = NULL,
	@AdditionalDescription VARCHAR(100) = NULL,
	@Price INT = NULL,
	@Quantity INT = NULL,
	@Size VARCHAR(5) = NULL,
	@Color VARCHAR(10) = NULL,
	@CompanyName VARCHAR(20) = NULL,
	@CategoryId INT = NULL,
	@Sold BIT = False,
	@IsCustomized BIT = False,
	@IsActive BIT = False
AS
BEGIN
	SET NOCOUNT ON;
	if (@Action = 'GETALL')
	BEGIN
		Select * From Product
	END
	if(@Action = 'GETBYID')
	BEGIN
	SELECT 
        p.ProductID,
        p.ProductName,
        p.ShortDescription,
        p.LongDescription,
        p.AdditionalDescription,
        p.Price,
        p.Quantity,
        p.Size,
        p.Color,
        p.CompanyName,
        p.CategoryId,
        p.Sold,
        p.IsCustiomized,
        p.IsActive,
        p.CreatedDate
    FROM 
        Product p
    WHERE 
        p.ProductID = @ProductID;
	END
	if (@Action = 'INSERT')
	BEGIN
	INSERT  INTO Product(ProductName,ShortDescription,LongDescription,AdditionalDescription,Price,Quantity,Size,Color,CompanyName,CategoryId,Sold,IsCustiomized,IsActive,CreatedDate)
	VALUES(@ProductName, @ShortDescription,@LongDescription,@AdditionalDescription,@Price,@Quantity,@Size,@Color,@CompanyName,@CategoryId,@Sold,@IsCustomized,@IsActive,GETDATE())
	END
	if (@Action = 'UPDATE')
	BEGIN
			UPDATE Product
    SET 
        ProductName = ISNULL(@ProductName, ProductName),
        ShortDescription = @ShortDescription,
        LongDescription = @LongDescription,
        AdditionalDescription = @AdditionalDescription,
        Price = @Price,
        Quantity = @Quantity,
        Size = @Size,
        Color = @Color,
        CompanyName = @CompanyName,
        CategoryId = @CategoryId,
        Sold = @Sold,
        IsCustiomized = @IsCustomized,
        IsActive = @IsActive
    WHERE ProductId = @ProductID;
	END
	if (@Action = 'DELETE')
	BEGIN
		DELETE From Product 
		WHERE ProductId = @ProductID
	END
	If (@Action = 'GETPRODUCTID')
	BEGIN
		SELECT TOP 1 ProductId FROM Product ORDER BY ProductId DESC
	END
	if (@Action = 'ACTIVEPRODUCT')
	BEGIN
		SELECT * From Product c
		WHERE c.IsActive = 1 ORDER BY c.ProductName
	END
	if (@Action = 'GETWITHIMAGE')
	BEGIN
	SELECT 
            p.ProductID,
            p.ProductName,
            p.ShortDescription,
            p.LongDescription,
            p.AdditionalDescription,
            p.Price,
            p.Quantity,
            p.Size,
            p.Color,
            p.CompanyName,
            p.CategoryId,
            p.Sold,
            p.IsCustiomized,
            p.IsActive,
            p.CreatedDate,
            pi.ImageUrl -- Include product image URL
        FROM 
            Product p
        LEFT JOIN 
            ProductImages pi ON p.ProductID = pi.ProductID -- Join with ProductImages table
        WHERE 
            p.IsActive = 1 AND p.CategoryId = @CategoryId -- Assuming you want only active products
        ORDER BY 
            p.ProductName;

	END
	if (@Action = 'GETPRODUCTDETAILS')
	BEGIN
	SELECT 
            p.ProductID,
            p.ProductName,
            p.ShortDescription,
            p.LongDescription,
            p.AdditionalDescription,
            p.Price,
            p.Quantity,
            p.Size,
            p.Color,
            p.CompanyName,
            p.CategoryId,
            p.Sold,
            p.IsCustiomized,
            p.IsActive,
            p.CreatedDate,
            pi.ImageUrl -- Include product image URL
        FROM 
            Product p
        LEFT JOIN 
            ProductImages pi ON p.ProductID = pi.ProductID -- Join with ProductImages table
        WHERE 
            p.IsActive = 1 AND p.ProductId = @ProductID -- Assuming you want only active products
        ORDER BY 
            p.ProductName;

	END
    END
GO
USE [master]
GO
ALTER DATABASE [JSM] SET  READ_WRITE 
GO
