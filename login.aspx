﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="JSM.WebForm1" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Login Page</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" />
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <style>
        body {
            background-color: #f5f5f5;
        }

       .container {
            max-width: 400px;
            margin: 40px auto;
            padding: 20px;
            background-color: #fff;
            border: 1px solid #ddd;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
        }

       .form-control {
            margin-bottom: 20px;
        }

       .btn-login {
            width: 100%;
            padding: 10px;
            font-size: 18px;
            font-weight: bold;
            color: #fff;
            background-color: #337ab7;
            border: none;
            border-radius: 5px;
            cursor: pointer;
        }

       .btn-login:hover {
            background-color: #23527c;
        }
    </style>
</head>
<body>
<div class="container">
        <h2 class="text-center">Login</h2>
        <form id="form1" runat="server">
            <div class="form-group">
                <label for="username">Username:</label>
                <asp:TextBox ID="txtUsername" runat="server" CssClass="form-control" />
            </div>
            <div class="form-group">
                <label for="password">Password:</label>
                <asp:TextBox ID="txtPassword" runat="server" CssClass="form-control" TextMode="Password" />
            </div>
            <asp:Button ID="btnLogin" runat="server" CssClass="btn-login" Text="Login"  />
        </form>
    </div>

    <script>
        $(document).ready(function () {
            $('#form1').submit(function (e) {
                e.preventDefault();
                var username = $('#txtUsername').val();
                var password = $('#txtPassword').val();

                if (username == "" || password == "") {
                    alert("Please enter both username and password");
                    return false;
                }

                // Call the server-side method to validate the credentials
                $.ajax({
                    type: "POST",
                    url: "Login.aspx/ValidateCredentials",
                    data: JSON.stringify({ username: username, password: password }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        if (data.d == true) {
                            window.location.href = "Default.aspx";
                        } else {
                            alert("Invalid username or password");
                        }
                    },
                    error: function (xhr, status, error) {
                        alert("Error: " + error);
                    }
                });
            });
        });
    </script>
</body>
</html>

